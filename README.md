# OMI_PolygonGridding 

This code takes OMI Level 2 files and grids them to 1x1 degree grid cells 
around given city centers. It calculates area weighted mean and median 
for the given grid cells. OMI data with cloud fraction > 30% or VCD Quality 
flag > 0 are not included in gridding (total number of possible OMI pixels
is still reported...Total Npts = Good Npts + Bad Npts). The data is output 
to a small CSV file. This can easily be adapted to a running window 
statistic, but here is given for a single day due to the size of the OMI NO2 
data files. 

