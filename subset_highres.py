import os
import numpy as np
from pylab import *
import h5py
import glob
import sys
import pandas as pd
from shapely.geometry import Polygon 
import time

#Importing global city information such as lat/lons
city_data = pd.read_csv('WorldCities_yy2.csv')

#Setting resolution to 1x1 degree
res = 1.

#Creating dictionary to store data
no2 = {}
for city in  city_data['City']:
    no2[city] = {}


#Looping through data files. Sample data is for a single day due to data size. 
for filename in glob.glob('InpData/*he5'):

    #Grabbing datetime information from filename 
    date_str = re.findall('OMNO2_[0-9]{4}m[0-9]{4}',filename)[0].replace('OMNO2_','').replace('m','')
    datetime_obj = datetime.datetime.strptime(date_str,'%Y%m%d')

    cur_doy = datetime_obj.timetuple().tm_yday
    cur_year = datetime_obj.year

    #Checking to see if date is in NO2 dictionary, if not, adding a new entry for the date. In this code
    #as we iterate, we store the NO2 values to calculate NO2 median, we also store weighted Sums to 
    #Calculate weighted mean
    for city in  city_data['City']:
        if date_str in no2[city]:
            continue
        no2[city][date_str] = {}
        no2[city][date_str]['Sum'] =0.
        no2[city][date_str]['NO2'] =[]
        no2[city][date_str]['SumSq'] = 0.
        no2[city][date_str]['Npt'] =0.
        no2[city][date_str]['Tot_Npt'] = 0.
        no2[city][date_str]['Wgt_Npt'] = 0.

    #Reading OMI Level 2 NO2 data
    f = h5py.File(filename,'r')
    
    omno2 = f['/HDFEOS/SWATHS/ColumnAmountNO2/Data Fields/ColumnAmountNO2Trop'][:]
    omno2[omno2 == -1.2676506E30] = np.nan
    #Setting data in row 46 to NaN due to possible row anomaly contamination that is not flagged properly 
    #in OMI L1b data
    omno2[:,46] = np.nan

    #Reading OMI effective cloud fraction (ECF), screening data with cloud fraction < 0.3
    ecf = np.array(f['HDFEOS/SWATHS/ColumnAmountNO2/Data Fields/CloudFraction'][:],np.float)
    ecf[ecf == -32767] = np.nan
    ecf = ecf * 0.001
    omno2[(ecf > 0.3) | (np.isnan(ecf)) ] = np.nan

    #Grabbing quality flag and screening out data with quality flag > 0
    vcd_qf = f['/HDFEOS/SWATHS/ColumnAmountNO2/Data Fields/VcdQualityFlags'][:]
    omno2[vcd_qf > 0] = np.nan

    #Grabbing lat/lon polygon corners from OMI NO2 data 
    corn_lat = f['/HDFEOS/SWATHS/ColumnAmountNO2/Geolocation Fields/FoV75CornerLatitude'][:]
    corn_lon = f['/HDFEOS/SWATHS/ColumnAmountNO2/Geolocation Fields/FoV75CornerLongitude'][:]

    #Grabbing lat/lon center points from OMI NO2 data 
    lat = f['/HDFEOS/SWATHS/ColumnAmountNO2/Geolocation Fields/Latitude'][:]
    lon = f['/HDFEOS/SWATHS/ColumnAmountNO2/Geolocation Fields/Longitude'][:]
    
    #Closing OMI NO2 file
    f.close()

    #Looping through cities to match OMI NO2 to the city polygon 
    for city in  city_data['City']:

        #Making polygon around city center with given resolution 
        lat_min = np.array(city_data['Latitude'])[np.array(city_data['City']) == city] - (res/2.)
        lat_max = np.array(city_data['Latitude'])[np.array(city_data['City']) == city] + (res/2.)
        lon_min = np.array(city_data['Longitude'])[np.array(city_data['City']) == city] - (res/2.)
        lon_max = np.array(city_data['Longitude'])[np.array(city_data['City']) == city] + (res/2.)
        city_poly = Polygon([(lon_min,lat_min),(lon_max,lat_min),(lon_max,lat_max),(lon_min,lat_max)])
        

        #Grabbing only OMI data within region of city polygon. Simply assuming that adding a 5 degree 
        #boundary around city center is enough to grab all OMI pixels 
        inds1, inds2, = np.where((lat > (lat_min-5.)) & (lat < (lat_max+5.)) & (lon < (lon_max+5. )) & (lon > (lon_min-5.)))
        
        #Looping through possible OMI polygon/city polygon matches 
        for i in range(len(inds1)):
            
            #Making polygon from OMI lat/lon corners 
            x0, y0 = corn_lon[0,inds1[i],inds2[i]], corn_lat[0,inds1[i],inds2[i]]
            x1, y1 = corn_lon[1,inds1[i],inds2[i]], corn_lat[1,inds1[i],inds2[i]]
            x2, y2 = corn_lon[2,inds1[i],inds2[i]], corn_lat[2,inds1[i],inds2[i]]
            x3, y3 = corn_lon[3,inds1[i],inds2[i]], corn_lat[3,inds1[i],inds2[i]]
            omi_poly = Polygon([(x0,y0),(x1,y1),(x2,y2),(x3,y3)])

            #Calculating intersection area of polygons. Added a try/except because at high extremely latitudes, 
            #OMI pixels can have odd shapes that shapely cannot calculate an area 
            try:
                area = (omi_poly.intersection(city_poly).area)/city_poly.area
            except:
                print('BAD OMI POLY ', omi_poly, city_poly)
                print(filename)
                continue 

            #Continuing on if area overlap is less than 0.1% 
            if (area < 0.001):
                continue 

            #Counting number of OMI polygons with > 0.1% area overlap including bad OMNO2. This helps in determining 
            #the percent of good samples for a given statistic 
            no2[city][date_str]['Tot_Npt'] = no2[city][date_str]['Tot_Npt'] + 1.

            #Continuing on if OMI NO2 value is fill 
            if np.isnan(omno2[inds1[i],inds2[i]]):
                continue
                
            #Storing NO2 values to eventually calculate weighted median. Here we calculate the median by simply adding 
            #the NO2 values to a new array. When doing this, we add int(1000.*overlap area) number of NO2 values to the 
            #new array, theoretically adding weight to the sampling for which we can then take a median 
            no2[city][date_str]['NO2'] = np.hstack((no2[city][date_str]['NO2'],[omno2[inds1[i],inds2[i]]]*int(area*1000.)))
            
            #Calculating weighted Sum for weighted mean calculation 
            no2[city][date_str]['Sum'] = no2[city][date_str]['Sum'] + (omno2[inds1[i],inds2[i]]*area)

            #Calculating weighted sum of squares in case standard deviation calculation is desired 
            no2[city][date_str]['SumSq'] = no2[city][date_str]['SumSq'] + (area*omno2[inds1[i],inds2[i]]*omno2[inds1[i],inds2[i]])

            #Storing number of weighted points for use in weighted mean calculation 
            no2[city][date_str]['Wgt_Npt'] = no2[city][date_str]['Wgt_Npt'] + area

            #Storing total number of good samples
            no2[city][date_str]['Npt'] = no2[city][date_str]['Npt'] + 1.



#Making dictionary to output data to a csv file 
out_csv = {}
out_csv['WeightedMedian'] = []
out_csv['WeightedMean'] = []
out_csv['TotalNpt'] = []
out_csv['city'] = []
out_csv['ScreenedNpt'] = []
out_csv['WeightedNpt'] = []
out_csv['year'] = []
out_csv['doy'] = []


#Looping through cities and dates to calculate statistics to go in output csv 
for city in no2.keys():
    for date_str in no2[city].keys():
        datetime_obj = datetime.datetime.strptime(date_str,'%Y%m%d')

        #Skipping data if weighted number of points is 0.
        if no2[city][date_str]['Wgt_Npt'] == 0.:
            continue

        #Calculating the weighted median with the newly created weighted array 
        out_csv['WeightedMedian'].append(np.nanpercentile(no2[city][date_str]['NO2'],50))

        #Calculating the weighted mean with weighted sum and number of points 
        out_csv['WeightedMean'].append(no2[city][date_str]['Sum']/no2[city][date_str]['Wgt_Npt'])

        #Storing number of points for user analysis 
        out_csv['TotalNpt'].append(np.array(no2[city][date_str]['Tot_Npt']))
        out_csv['WeightedNpt'].append(np.array(no2[city][date_str]['Wgt_Npt']))
        out_csv['ScreenedNpt'].append(np.array(no2[city][date_str]['Npt']))

        #Storing date/city information 
        out_csv['doy'].append(datetime_obj.timetuple().tm_yday)
        out_csv['year'].append(datetime_obj.year)
        out_csv['city'].append(city)


#Saving data to CSV file 
out_filename = 'SampleNO2Averaging.csv'
if os.path.exists(out_filename):
    os.remove(out_filename)
df = pd.DataFrame(data=out_csv)
df.to_csv(out_filename,columns=['city','year','doy','TotalNpt','ScreenedNpt','WeightedNpt','WeightedMedian','WeightedMean'], index=False)
    
